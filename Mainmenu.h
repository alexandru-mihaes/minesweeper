//
// Created by alexunder on 1/7/17.
//

#ifndef MINESWEEPER_MAINMENU_H
#define MINESWEEPER_MAINMENU_H

void mainmenu();
void newgame();
void newgameOptions(bool &);
void help();
void about();

#endif //MINESWEEPER_MAINMENU_H
