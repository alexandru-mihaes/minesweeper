# Minesweeper

This is a clone of Minesweeper. It is made in C++ using the ncurses library.

## Features

* Random generator of maps
* The choice of selecting the number of lines, columns and mines
* The ability to play with a mouse

You can get a glimpse of the game through the GIF below:

![alt tag](doc/image/intro.gif)

## Build and run

To run the game the following packages are required:

`libncurses5-dev libncursesw5-dev`

If you want to build run the following line (you need `G++`):

`make`

Run the game with: `./minesweeper`