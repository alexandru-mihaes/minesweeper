//
// Created by alexunder on 1/8/17.
//

#ifndef MINESWEEPER_FLAGS_H
#define MINESWEEPER_FLAGS_H

#include "Minesweeper.h"

void printFlags(Minesweeper *, int);
void printRestFlags(Minesweeper *);

#endif //MINESWEEPER_FLAGS_H
