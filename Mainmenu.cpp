//
// Created by alexunder on 1/7/17.
//

#include <ncurses.h>
#include <unistd.h>
#include "Newgame.h"

void about()
{
    int curChar;
    while (true) {
        clear();
        move(0, 2);
        printw("About\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n"
                       "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                       "┃ Copyright (C) 2017 Alexandru Mihăeș                                  ┃\n"
                       "┃                                                                      ┃\n"
                       "┃ This program is free software: you can redistribute it and/or modify ┃\n"
                       "┃ it under the terms of the GNU General Public License as published by ┃\n"
                       "┃ the Free Software Foundation, either version 3 of the License, or    ┃\n"
                       "┃ (at your option) any later version.                                  ┃\n"
                       "┃                                                                      ┃\n"
                       "┃ This program is distributed in the hope that it will be useful,      ┃\n"
                       "┃ but WITHOUT ANY WARRANTY; without even the implied warranty of       ┃\n"
                       "┃ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        ┃ \n"
                       "┃ GNU General Public License for more details.                         ┃\n"
                       "┃                                                                      ┃\n"
                       "┃ You should have received a copy of the GNU General Public License    ┃\n"
                       "┃ along with this program. If not, see <http://www.gnu.org/licenses/>. ┃\n"
                       "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
        curChar = getch();
        if (curChar == 'q' || curChar == 'Q')
            break;
        else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK)
                if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                    break;
        }
    }
}

void help()
{
    int curChar;
    int choiceTab = 13;
    init_pair(13, COLOR_BLUE, COLOR_BLACK);
    init_pair(14, COLOR_WHITE, COLOR_BLACK);
    while (true) {
        clear();
        move(0, 2);
        printw("Help\n━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        if (choiceTab == 13) {
            printw("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                           "┃                                         ┃\n"
                           "┣━━━━━━━━━━┳━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━┫\n"
                           "┃ Keyboard ┃ Mouse        ┃ Action        ┃\n"
                           "┣━━━━━━━━━━╋━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┫\n"
                           "┃ ENTER    ┃ Left click   ┃ select        ┃\n"
                           "┃ F        ┃ Right click  ┃ flag / unflag ┃\n"
                           "┃ Q        ┃ Middle click ┃ back          ┃\n"
                           "┃ W        ┃              ┃ move up       ┃\n"
                           "┃ A        ┃              ┃ move left     ┃\n"
                           "┃ S        ┃              ┃ move down     ┃\n"
                           "┃ D        ┃              ┃ move right    ┃\n"
                           "┗━━━━━━━━━━┻━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━━┛\n");
            printw("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                           "┃                                                                       ┃\n"
                           "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
        } else
            printw("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                           "┃                                         ┃\n"
                           "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
        if (choiceTab == 14) {
            move(5, 0);
            printw("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                           "┃                                                                       ┃");
            move(7, 0);
            printw("┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫\n"
                           "┃ The objective in Minesweeper is to find and mark all the mines hidden ┃\n"
                           "┃ under the grey squares, in the shortest time possible. This is done   ┃\n"
                           "┃ by pressing on squares to open them. Each square will have one of the ┃\n"
                           "┃ following:                                                            ┃\n"
                           "┃   ➤ A mine, and if you press on it you'll lose the game.              ┃\n"
                           "┃   ➤ A number, which tells you how many of its adjacent squares have   ┃\n"
                           "┃   mines in them.                                                      ┃\n"
                           "┃   ➤ Nothing. In this case you know that none of the adjacent squares  ┃\n"
                           "┃   have mines, and they will be automatically opened as well.          ┃\n"
                           "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛\n");
        }
        attron(COLOR_PAIR(13));
        mvaddstr(3, 2, "Controls");
        attroff(COLOR_PAIR(13));
        attron(COLOR_PAIR(14));
        if (choiceTab == 13)
            mvaddstr(16, 2, "How to play");
        else
            mvaddstr(6, 2, "How to play");
        attroff(COLOR_PAIR(14));
        curChar = getch();
        if (curChar == 'q' || curChar == 'Q')
            break;
        else if (curChar == 'w' || curChar == 'W' && choiceTab == 14) {
            CONTROLS_TAB:
            init_pair(choiceTab, COLOR_WHITE, COLOR_BLACK);
            choiceTab = 13;
            init_pair(choiceTab, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == 's' || curChar == 'S') {
            HOWTOPLAY_TAB:
            init_pair(choiceTab, COLOR_WHITE, COLOR_BLACK);
            choiceTab = 14;
            init_pair(choiceTab, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK)
                if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                    break;
                else if (event.bstate & BUTTON1_CLICKED) {
                    if (event.y == 3 && event.x <= 42 && choiceTab == 14)
                        goto CONTROLS_TAB;
                    else if (event.y == 16 && event.x <= 72 && choiceTab == 13)
                        goto HOWTOPLAY_TAB;
                }
        }
    }
}

void newgameOptions(bool &pressedQ)
{
    int linesNr = 8, columnsNr = 8, minesNr = 10;
    int curChar;
    int choice = 12;
    init_pair(6, COLOR_WHITE, COLOR_BLACK);
    init_pair(7, COLOR_WHITE, COLOR_BLACK);
    init_pair(8, COLOR_WHITE, COLOR_BLACK);
    init_pair(9, COLOR_WHITE, COLOR_BLACK);
    init_pair(10, COLOR_WHITE, COLOR_BLACK);
    init_pair(11, COLOR_WHITE, COLOR_BLACK);
    init_pair(12, COLOR_BLUE, COLOR_BLACK);
    while (true) {
        clear();
        attron(COLOR_PAIR(5));
        move(0, 7);
        printw("Start new game\n━━━━━━━━━━━━━━━━━━━━━━━━━━━\n");
        printw("┏━━━━━━━┳━━━━━━━━━┳━━━━━━━┓\n"
                       "┃ Lines ┃ Columns ┃ Mines ┃\n"
                       "┣━━━━━━━╋━━━━━━━━━╋━━━━━━━┫\n"
                       "┃       ┃         ┃       ┃\n"
                       "┃       ┃         ┃       ┃\n"
                       "┃       ┃         ┃       ┃\n"
                       "┗━━━━━━━┻━━━━━━━━━┻━━━━━━━┛\n"
                       "          ┏━━━━┓\n"
                       "          ┃    ┃\n"
                       "          ┗━━━━┛\n");
        attroff(COLOR_PAIR(5));
        attron(COLOR_PAIR(6));
        mvprintw(5, 4, "+");
        attroff(COLOR_PAIR(6));
        attron(COLOR_PAIR(7));
        mvprintw(5, 13, "+");
        attroff(COLOR_PAIR(7));
        attron(COLOR_PAIR(8));
        mvprintw(5, 22, "+");
        attroff(COLOR_PAIR(8));
        attron(COLOR_PAIR(5));
        move(6, 4);
        printw("%d", linesNr);
        move(6, 13);
        printw("%d", columnsNr);
        if (minesNr < 100)
            move(6, 22);
        else
            move(6, 21);
        printw("%d", minesNr);
        attroff(COLOR_PAIR(5));
        attron(COLOR_PAIR(9));
        mvprintw(7, 4, "-");
        attroff(COLOR_PAIR((9)));
        attron(COLOR_PAIR(10));
        mvprintw(7, 13, "-");
        attroff(COLOR_PAIR(10));
        attron(COLOR_PAIR(11));
        mvprintw(7, 22, "-");
        attroff(COLOR_PAIR(11));
        attron(COLOR_PAIR(12));
        move(10, 12);
        printw("OK");
        attroff(COLOR_PAIR(12));
        curChar = getch();
        if ((curChar == 'w' || curChar == 'W') && choice > 8) {
            init_pair(choice, COLOR_WHITE, COLOR_BLACK);
            if (choice == 12)
                choice = 10;
            else
                choice -= 3;
            init_pair(choice, COLOR_BLUE, COLOR_BLACK);
        } else if ((curChar == 'a' || curChar == 'A') && choice != 6 && choice != 9 && choice != 12) {
            init_pair(choice, COLOR_WHITE, COLOR_BLACK);
            choice -= 1;
            init_pair(choice, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == 's' || curChar == 'S') {
            init_pair(choice, COLOR_WHITE, COLOR_BLACK);
            if (choice <= 8)
                choice += 3;
            else
                choice = 12;
            init_pair(choice, COLOR_BLUE, COLOR_BLACK);
        } else if ((curChar == 'd' || curChar == 'D') && choice != 8 && choice != 11 && choice != 12) {
            init_pair(choice, COLOR_WHITE, COLOR_BLACK);
            choice += 1;
            init_pair(choice, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == 'q' || curChar == 'Q') {
            GO_BACK:
            pressedQ = true;
            break;
        } else if (curChar == '\n') {
            if (choice < 12) {
                switch (choice) {
                    case 6:
                    PLUS_LINES:
                        if (linesNr < MAX_CELLS - 1)
                            linesNr++;
                        break;
                    case 7:
                    PLUS_COLUMNS:
                        if (columnsNr < MAX_CELLS - 1)
                            columnsNr++;
                        break;
                    case 8:
                    PLUS_MINES:
                        if (minesNr < linesNr * columnsNr - 10)
                            minesNr++;
                        break;
                    case 9:
                    MINUS_LINES:
                        if (linesNr > 4)
                            linesNr--;
                        break;
                    case 10:
                    MINUS_COLUMNS:
                        if (columnsNr > 4)
                            columnsNr--;
                        break;
                    case 11:
                    MINUS_MINES:
                        if (minesNr > 1)
                            minesNr--;
                        break;
                    default:
                        break;
                }
                if (minesNr > linesNr * columnsNr - 10)
                    minesNr = linesNr * columnsNr - 10;
            } else // pressed ok
                opengame(linesNr, columnsNr, minesNr);
        } else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK) {
                if (event.bstate & BUTTON1_CLICKED) {
                    if (event.y == 5 && event.x == 4) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 6;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto PLUS_LINES;
                    } else if (event.y == 5 && event.x == 13) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 7;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto PLUS_COLUMNS;
                    } else if (event.y == 5 && event.x == 22) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 8;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto PLUS_MINES;
                    } else if (event.y == 7 && event.x == 4) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 9;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto MINUS_LINES;
                    } else if (event.y == 7 && event.x == 13) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 10;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto MINUS_COLUMNS;
                    } else if (event.y == 7 && event.x == 22) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 11;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        goto MINUS_MINES;
                    } else if (event.y == 10 && (event.x == 12 || event.x == 13)) {
                        init_pair(choice, COLOR_WHITE, COLOR_BLACK);
                        choice = 12;
                        init_pair(choice, COLOR_BLUE, COLOR_BLACK);
                        refresh();
                        usleep(250000);
                        opengame(linesNr, columnsNr, minesNr);
                    }
                }
                if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                    goto GO_BACK;
                refresh();
            }
        }
    }
}

void newgame()
{
    clear();
    bool pressedQ = false;
    newgameOptions(pressedQ);
    curs_set(0);
}

void mainmenu()
{
    curs_set(0);
    int curChar;
    init_pair(1, COLOR_BLUE, COLOR_BLACK);
    init_pair(2, COLOR_WHITE, COLOR_BLACK);
    init_pair(3, COLOR_WHITE, COLOR_BLACK);
    init_pair(4, COLOR_WHITE, COLOR_BLACK);
    init_pair(5, COLOR_WHITE, COLOR_BLACK);
    int choiceMain = 1;
    while (true) {
        clear();
        attron(COLOR_PAIR(5));
        move(0, 4);
        printw("Minesweeper\n━━━━━━━━━━━━━━━━━━━━\n");
        for (int i = 1; i <= 4; i++) {
            printw("┏━━━━━━━━━━━━━━━━━━┓\n");
            printw("┃                  ┃\n");
            printw("┗━━━━━━━━━━━━━━━━━━┛\n");
        }
        attroff(COLOR_PAIR(5));
        attron(COLOR_PAIR(1));
        mvaddstr(3, 3, "Start new game");
        attroff(COLOR_PAIR(1));
        attron(COLOR_PAIR(2));
        mvaddstr(6, 3, "    Help");
        attroff(COLOR_PAIR(2));
        attron(COLOR_PAIR(3));
        mvaddstr(9, 3, "    About");
        attroff(COLOR_PAIR(3));
        attron(COLOR_PAIR(4));
        mvaddstr(12, 3, "    Quit");
        attroff(COLOR_PAIR(4));
        refresh();
        curChar = getch();
        if ((curChar == 'w' || curChar == 'W') && choiceMain != 1) {
            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
            choiceMain--;
            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
        } else if ((curChar == 's' || curChar == 'S') && choiceMain != 4) {
            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
            choiceMain++;
            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == 'q' || curChar == 'Q') {
            endwin();
            break;
        } else if (curChar == '\n') {
            if (choiceMain == 1)
                newgame();
            else if (choiceMain == 2)
                help();
            else if (choiceMain == 3)
                about();
            else if (choiceMain == 4) {
                endwin();
                break;
            }
        } else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK) {
                if (event.bstate & BUTTON1_CLICKED) {
                    if (event.x <= 19) {
                        if (event.y == 3) {
                            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
                            choiceMain = 1;
                            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
                            refresh();
                            usleep(500000);
                            newgame();
                        } else if (event.y == 6) {
                            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
                            choiceMain = 2;
                            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
                            refresh();
                            usleep(500000);
                            help();
                        } else if (event.y == 9) {
                            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
                            choiceMain = 3;
                            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
                            refresh();
                            usleep(500000);
                            about();
                        }
                        if (event.y == 12) {
                            QUIT_BY_MOUSE:
                            init_pair(choiceMain, COLOR_WHITE, COLOR_BLACK);
                            choiceMain = 4;
                            init_pair(choiceMain, COLOR_BLUE, COLOR_BLACK);
                            refresh();
                            usleep(250000);
                            endwin();
                            break;
                        }
                    }
                } else if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                    goto QUIT_BY_MOUSE;
            }
        }
    }
}