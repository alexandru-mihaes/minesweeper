//
// Created by alexunder on 1/7/17.
//

#include <ncurses.h>
#include <unistd.h>
#include "Minesweeper.h"
#include "Flags.h"

void printNearMinesColor(int line, int column, char mines)
{
    init_pair(13, COLOR_GREEN, COLOR_BLACK);
    init_pair(14, COLOR_YELLOW, COLOR_BLACK);
    init_pair(15, COLOR_RED, COLOR_BLACK);
    if (mines == '1') {
        attron(COLOR_PAIR(13));
        mvaddch((line - 1) * 2 + 1, (column - 1) * 2 + 1, mines);
        attroff(COLOR_PAIR(13));
    } else if (mines == '2') {
        attron(COLOR_PAIR(14));
        mvaddch((line - 1) * 2 + 1, (column - 1) * 2 + 1, mines);
        attroff(COLOR_PAIR(14));
    } else {
        attron(COLOR_PAIR(15));
        mvaddch((line - 1) * 2 + 1, (column - 1) * 2 + 1, mines);
        attroff(COLOR_PAIR(15));
    }
}

void printAreYouSure(bool &quit, int linesNr)
{
    curs_set(0);
    int curChar;
    int choiceQuit = 1;
    init_pair(16, COLOR_BLUE, COLOR_BLACK);
    init_pair(17, COLOR_WHITE, COLOR_BLACK);
    while (true) {
        mvaddstr(linesNr * 2 + 1, 0, "Are you sure you want to quit?");
        attron(COLOR_PAIR(16));
        mvaddstr(linesNr * 2 + 2, 0, "No");
        attroff(COLOR_PAIR(16));
        attron(COLOR_PAIR(17));
        mvaddstr(linesNr * 2 + 2, 3, "Yes");
        attroff(COLOR_PAIR(17));
        curChar = getch();
        if (curChar == 'a' || curChar == 'A') {
            init_pair(17, COLOR_WHITE, COLOR_BLACK);
            choiceQuit = 1;
            init_pair(16, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == 'd' || curChar == 'D') {
            init_pair(16, COLOR_WHITE, COLOR_BLACK);
            choiceQuit = 2;
            init_pair(17, COLOR_BLUE, COLOR_BLACK);
        } else if (curChar == '\n') {
            if (choiceQuit == 2) {
                quit = true;
                break;
            } else
                break;
        } else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK) {
                if (event.bstate & BUTTON1_CLICKED) {
                    if (event.y == linesNr * 2 + 2 && (event.x == 0 || event.x == 1)) {
                        init_pair(17, COLOR_WHITE, COLOR_BLACK);
                        init_pair(16, COLOR_BLUE, COLOR_BLACK);
                        refresh();
                        usleep(250000);
                        break;
                    } else if (event.y == linesNr * 2 + 2 && (event.x == 4 || event.x == 6)) {
                        init_pair(16, COLOR_WHITE, COLOR_BLACK);
                        init_pair(17, COLOR_BLUE, COLOR_BLACK);
                        refresh();
                        usleep(250000);
                        quit = true;
                        break;
                    }
                }
            }
        }
        refresh();
    }
    move(linesNr * 2 + 1, 0);
    for (int i = 0; i <= 29; i++)
        printw(" ");
    attron(COLOR_PAIR(15));
    mvaddstr(linesNr * 2 + 2, 0, "  ");
    attroff(COLOR_PAIR(15));
    attron(COLOR_PAIR(16));
    mvaddstr(linesNr * 2 + 2, 3, "   ");
    attroff(COLOR_PAIR(16));
    curs_set(1);
}

void printGrid(Minesweeper *game)
{
    printw("┌");
    for (int i = 2; i <= game->getColumnsNr(); i++) {
        printw("─┬");
    }
    printw("─┐");
    printw("\n");
    for (int i = 1; i <= game->getLinesNr(); i++) {
        for (int j = 1; j <= game->getColumnsNr(); j++) {
            printw("│•");
            if (game->getColumnsNr() == j) {
                if (game->getLinesNr() == i)
                    goto END_OF_FIELD;
                printw("│\n├");
                for (int k = 2; k <= game->getColumnsNr(); k++) {
                    printw("─┼");
                }
                printw("─┤\n");
            }
        }
    }
    END_OF_FIELD:
    printw("│\n└");
    for (int i = 2; i <= game->getColumnsNr(); i++) {
        printw("─┴");
    }
    printw("─┘");
}

void printYouWon(Minesweeper *game)
{
    printRestFlags(game);
    curs_set(0);
    init_pair(19, COLOR_BLUE, COLOR_BLACK);
    attron(COLOR_PAIR(19));
    mvaddstr(game->getLinesNr() * 2 + 1, 0, "You won!");
    attroff(COLOR_PAIR(19));
    mvaddstr(game->getLinesNr() * 2 + 2, 0, "Press any key to contiune...");
    refresh();
    getch();
    endwin();
}

void refreshTheScreen(Minesweeper *game)
{
    clear();
    move(0, 0);
    printGrid(game);
    for (int i = 1; i <= game->getLinesNr(); i++)
        for (int j = 1; j <= game->getColumnsNr(); j++)
            if (game->getUserField(i, j) == -1)
                mvaddch((i - 1) * 2 + 1, (j - 1) * 2 + 1, ' ');
            else if (game->getUserField(i, j) >= 1 && game->getUserField(i, j) <= 8)
                printNearMinesColor(i, j, game->getUserField(i, j) + '0');
            else if (game->getUserField(i, j) == 10)
                mvaddstr((i - 1) * 2 + 1, (j - 1) * 2 + 1, "⚑");
            else if (game->getUserField(i, j) == 0)
                mvaddstr((i - 1) * 2 + 1, (j - 1) * 2 + 1, "•");
    printFlags(game, 0);
}

bool clickIsValid(Minesweeper *game, MEVENT event)
{
    if (event.y != 0 && event.x != 0)
        if (event.y / 2 + 1 >= 1 && event.y / 2 + 1 <= game->getLinesNr() &&
            event.x / 2 + 1 >= 1 && event.x / 2 + 1 <= game->getColumnsNr() &&
            event.y % 2 == 1 && event.x % 2 == 1)
            return true;
    return false;
}

void moveCursor(Minesweeper *game)
{
    bool firstSquare = true, gameOver = false;
    int curChar;
    printFlags(game, 0);
    move(game->curY, game->curX);
    refresh();
    while (true) {
        if (curChar == KEY_RESIZE) {
            refreshTheScreen(game);
        }
        move(game->curY, game->curX);
        refresh();
        curChar = getch();
        if (curChar == 'w' || curChar == 'W') {
            if (game->curY > 1)
                game->curY -= 2;
        } else if (curChar == 'a' || curChar == 'A') {
            if (game->curX > 1)
                game->curX -= 2;
        } else if (curChar == 's' || curChar == 'S') {
            if (game->curY < game->getLinesNr() * 2 - 1)
                game->curY += 2;
        } else if (curChar == 'd' || curChar == 'D') {
            if (game->curX < game->getColumnsNr() * 2 - 1)
                game->curX += 2;
        } else if (curChar == KEY_MOUSE) {
            MEVENT event;
            if (getmouse(&event) == OK) {
                if (event.bstate & BUTTON1_CLICKED) {
                    if (clickIsValid(game, event)) {
                        game->curY = event.y;
                        game->curX = event.x;
                        curChar = '\n';
                    }
                } else if (event.bstate & BUTTON3_CLICKED) {
                    if (clickIsValid(game, event)) {
                        game->curY = event.y;
                        game->curX = event.x;
                        curChar = 'f';
                    }
                } else if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                    goto ARE_YOU_SURE;
            }
        }
        printFlags(game, curChar);
        if (curChar == '\n' &&
            game->getFlag(game->curY / 2 + 1, game->curX / 2 + 1) == 0 &&
            game->getFieldPushed(game->curY / 2 + 1, game->curX / 2 + 1) == 0) {
            if (firstSquare &&
                (game->getField(game->curY / 2 + 1, game->curX / 2 + 1) == 1 ||
                 game->nearMines(game->curY / 2 + 1, game->curX / 2 + 1) != 0)) {
                game->openFirstArea(game->curY / 2 + 1, game->curX / 2 + 1);
                firstSquare = false;
            }
            game->reveal(game->curY / 2 + 1, game->curX / 2 + 1, gameOver);
            firstSquare = false;
        }
        move(game->curY, game->curX);
        if (gameOver) {
            init_pair(18, COLOR_RED, COLOR_BLACK);
            attron(COLOR_PAIR(18));
            printw("💣");
            attroff(COLOR_PAIR(18));
            move(game->curY, game->curX);
            while (true) {
                mvaddstr(game->getLinesNr() * 2 + 1, 0, "Press Q / middle click to contiune...");
                curChar = getch();
                if (curChar == 'q' || curChar == 'Q')
                    break;
                else if (curChar == KEY_MOUSE) {
                    MEVENT event;
                    if (getmouse(&event) == OK)
                        if ((event.bstate & BUTTON2_CLICKED) || (event.bstate & BUTTON2_PRESSED))
                            break;
                }
            }
            break;
        }
        refresh();
        if (curChar == 'q' || curChar == 'Q') {
            ARE_YOU_SURE:
            bool quit = false;
            printAreYouSure(quit, game->getLinesNr());
            if (quit)
                break;
            move(game->curY, game->curX);
            refreshTheScreen(game);
        }
        if (game->getRevealedSquares() == game->getLinesNr() * game->getColumnsNr() - game->getMinesNr()) {
            printYouWon(game);
            break;
        }
        refresh();
    }
}

void opengame(int linesNr, int columnsNr, int minesNr)
{
    clear();
    Minesweeper *game = new Minesweeper(linesNr, columnsNr, minesNr);
    curs_set(1);
    printGrid(game);
    moveCursor(game);
    delete game;
    curs_set(0);
}
