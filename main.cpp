#include <ncurses.h>
#include <iostream>
#include "Mainmenu.h"

int main()
{
    setlocale(LC_ALL, "");
    initscr();
    start_color();
    noecho();
    keypad(stdscr, TRUE);
    mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);
    mainmenu();
    endwin();
    return 0;
}