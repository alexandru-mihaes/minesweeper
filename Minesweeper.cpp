//
// Created by AlexUnder on 2 January, 2017.
//

#include <ncurses.h>
#include <ctime>
#include <cstdlib>
#include "Minesweeper.h"
#include "Newgame.h"

using namespace std;

Minesweeper::Minesweeper(int l, int c, int m) : linesNr(l), columnsNr(c), minesNr(m)
{
    generateField();
}

Minesweeper::~Minesweeper()
{
}

void Minesweeper::generateField()
{
    srand(time(NULL));
    int *tempNrMines;
    tempNrMines = new int(0);
    while ((*tempNrMines) != minesNr) {
        int i, j;
        do {
            i = rand() % linesNr + 1;
            j = rand() % columnsNr + 1;
        } while (field[i][j] == MINE);
        field[i][j] = MINE;
        (*tempNrMines)++;
    }
    delete tempNrMines;
}

int Minesweeper::getLinesNr()
{
    return linesNr;
}

int Minesweeper::getColumnsNr()
{
    return columnsNr;
}

int Minesweeper::getMinesNr()
{
    return minesNr;
}

int Minesweeper::getField(int l, int c)
{
    return field[l][c];
}

int Minesweeper::getRevealedSquares()
{
    return revealedSquares;
}

int Minesweeper::getFieldPushed(int l, int c)
{
    return fieldPushed[l][c];
}

int Minesweeper::getFlag(int l, int c)
{
    return flags[l][c];
}

int Minesweeper::getUserField(int l, int c)
{
    return userField[l][c];
}

void Minesweeper::setFlag(int l, int c)
{
    flags[l][c] = 1;
    userField[l][c] = 10;
    flagsUsed++;
}

void Minesweeper::unflag(int l, int c)
{
    flags[l][c] = 0;
    userField[l][c] = 0;
    flagsUsed--;
}

int Minesweeper::getFlagsUsed()
{
    return flagsUsed;
}

void Minesweeper::openFirstArea(int l, int c)
{
    if (field[l][c] == 1)
        moveMine(l, c, l, c);
    for (int d = 1; d <= 8; d++) {
        int in = l + directionLine[d];
        int jn = c + directionColumn[d];
        if (field[in][jn] == 1)
            moveMine(in, jn, l, c);
    }
}

void Minesweeper::moveMine(int in, int jn, int l, int c)
{
    bool moved = false;
    for (int i = 1; i <= linesNr && !moved; i++)
        for (int j = 1; j <= columnsNr && !moved; j++)
            if (field[i][j] == 0 && i != l && j != c) {
                bool neighbor = false;
                for (int d = 1; d <= 8; d++) {
                    int neighborL = l + directionLine[d];
                    int neighborC = c + directionColumn[d];
                    if (neighborL == i && neighborC == j)
                        neighbor = true;
                }
                if (!neighbor) {
                    field[i][j] = 1;
                    moved = true;
                }
            }
    field[in][jn] = 0;
}

void Minesweeper::reveal(int line, int column, bool &gameOver)
{
    if (field[line][column] == 0) {
        int mines = nearMines(line, column);
        if (mines != 0) { // dangerous position
            fieldPushed[line][column] = 1;
            revealedSquares++;
            userField[line][column] = mines;
            mines = mines + '0';
            printNearMinesColor(line, column, mines);
        } else // safe position
            DFS(line, column);
    } else // game over
    {
        gameOver = true;
        revealTheField();
    }
}

int Minesweeper::nearMines(int line, int column)
{
    int mines = 0, e, ib, jb;
    for (e = 1; e <= 8; e++) {
        ib = line + directionLine[e];
        jb = column + directionColumn[e];
        if (field[ib][jb] == 1)
            mines++;
    }
    return mines;
}

void Minesweeper::revealTheField()
{
    for (int i = 1; i <= linesNr; i++)
        for (int j = 1; j <= columnsNr; j++)
            if (field[i][j] == 1)
                mvaddstr((i - 1) * 2 + 1, (j - 1) * 2 + 1, "💣");
}

void Minesweeper::DFS(int i, int j)
{
    userField[i][j] = -1;
    fieldPushed[i][j] = 1;
    revealedSquares++;
    mvaddch((i - 1) * 2 + 1, (j - 1) * 2 + 1, ' ');
    for (int d = 1; d <= 8; d++) {
        int in = i + directionLine[d];
        int jn = j + directionColumn[d];
        if (field[in][jn] == 0 && fieldPushed[in][jn] == 0 && inField(in, jn)) {
            fieldPushed[in][jn] = 1;
            int mines = nearMines(in, jn);
            if (mines != 0) { // dangerous position
                revealedSquares++;
                userField[in][jn] = mines;
                mines = mines + '0';
                printNearMinesColor(in, jn, mines);
            } else // safe position
            {
                mvaddch((i - 1) * 2 + 1, (j - 1) * 2 + 1, ' ');
                DFS(in, jn);
            }
        }
    }
}

int Minesweeper::inField(int in, int jn)
{
    if (in >= 1 && in <= linesNr && jn >= 1 && jn <= columnsNr)
        return 1;
    else
        return 0;
}