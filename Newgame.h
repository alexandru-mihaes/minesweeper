//
// Created by alexunder on 1/7/17.
//

#ifndef MINESWEEPER_NEWGAME_H
#define MINESWEEPER_NEWGAME_H

#include "Minesweeper.h"
#include <ncurses.h>

void opengame(int, int, int);
void moveCursor(Minesweeper *);
void printGrid(Minesweeper *);
void printNearMinesColor(int, int, char);
void printAreYouSure(bool &, int);
void printYouWon(Minesweeper *);
void refreshTheScreen(Minesweeper *);
bool clickIsValid(Minesweeper *, MEVENT);

#endif //MINESWEEPER_NEWGAME_H
