//
// Created by alexunder on 1/8/17.
//

#include <ncurses.h>
#include "Minesweeper.h"

void printFlags(Minesweeper *game, int curChar)
{
    mvaddstr(1, game->getColumnsNr() * 2 + 5, "⚑");
    move(2, game->getColumnsNr() * 2 + 2);
    if (game->getFlagsUsed() < 10)
        printw("  %d", game->getFlagsUsed());
    else if (game->getFlagsUsed() < 100)
        printw(" %d", game->getFlagsUsed());
    else
        printw("%d", game->getFlagsUsed());
    mvaddstr(2, game->getColumnsNr() * 2 + 5, "/");
    move(2, game->getColumnsNr() * 2 + 6);
    printw("%d", game->getMinesNr());
    if ((curChar == 'f' || curChar == 'F') && game->getFieldPushed(game->curY / 2 + 1, game->curX / 2 + 1) == 0) {
        if (game->getFlag(game->curY / 2 + 1, game->curX / 2 + 1) == 0) { // set flag
            if (game->getFlagsUsed() < game->getMinesNr()) {
                mvaddstr(game->curY, game->curX, "⚑");
                game->setFlag(game->curY / 2 + 1, game->curX / 2 + 1);
                move(2, game->getColumnsNr() * 2 + 2);
                if (game->getFlagsUsed() < 10)
                    printw("  %d", game->getFlagsUsed());
                else if (game->getFlagsUsed() < 100)
                    printw(" %d", game->getFlagsUsed());
                else
                    printw("%d", game->getFlagsUsed());
            }
        } else { // unflag
            mvaddstr(game->curY, game->curX, "•");
            game->unflag(game->curY / 2 + 1, game->curX / 2 + 1);
            move(2, game->getColumnsNr() * 2 + 2);
            if (game->getFlagsUsed() < 10)
                printw("  %d", game->getFlagsUsed());
            else if (game->getFlagsUsed() < 100)
                printw(" %d", game->getFlagsUsed());
            else
                printw("%d", game->getFlagsUsed());
        }
        move(game->curY, game->curX);
        refresh();
    }
}

void printRestFlags(Minesweeper *game)
{
    for (int i = 1; i <= game->getLinesNr(); i++)
        for (int j = 1; j <= game->getColumnsNr(); j++)
            if (game->getField(i, j) == 1 && game->getFlag(i, j) == 0) {
                move(i * 2 - 1, j * 2 - 1);
                game->curY = i * 2 - 1;
                game->curX = j * 2 - 1;
                printFlags(game, 'f');
                refresh();
            }
}

