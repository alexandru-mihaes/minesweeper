//
// Created by AlexUnder on 2 January, 2017.
//

#ifndef MINESWEEPER_MINESWEEPER_H
#define MINESWEEPER_MINESWEEPER_H

#define MAX_CELLS 21
#define MINE 1

class Minesweeper {
    public:
        Minesweeper(int, int, int);
        ~Minesweeper();
        void generateField();
        int getLinesNr();
        int getColumnsNr();
        int getMinesNr();
        int getField(int, int);
        int getRevealedSquares();
        int getFieldPushed(int, int);
        int getFlag(int, int);
        int getUserField(int, int);
        void setFlag(int, int);
        void unflag(int, int);
        int getFlagsUsed();
        void openFirstArea(int, int);
        void reveal(int, int, bool &);
        int nearMines(int, int);
        int curX = 1, curY = 1;
    private:
        int linesNr = 0, columnsNr = 0, minesNr = 0, flagsUsed = 0, revealedSquares = 0;
        int field[MAX_CELLS][MAX_CELLS] = {}, fieldPushed[MAX_CELLS][MAX_CELLS] = {};
        int flags[MAX_CELLS][MAX_CELLS] = {}, userField[MAX_CELLS][MAX_CELLS] = {};
        const int directionLine[9] = {0, -1, -1, 0, 1, 1, 1, 0, -1};
        const int directionColumn[9] = {0, 0, 1, 1, 1, 0, -1, -1, -1};
        void moveMine(int, int, int, int);
        void revealTheField();
        void DFS(int, int);
        int inField(int, int);
};

#endif //MINESWEEPER_MINESWEEPER_H
